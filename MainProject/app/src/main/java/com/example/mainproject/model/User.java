package com.example.mainproject.model;

public class User {
    private int id;
    private String name;
    private String email;
    private String password;
    private String username;
    public User(String name,String username,String email){
        this.name=name;
        this.username=username;
        this.email=email;
    }
    public User(){

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    public String getUsername(){return username; }
    public void setUsername(String username){ this.username=username;}
}
