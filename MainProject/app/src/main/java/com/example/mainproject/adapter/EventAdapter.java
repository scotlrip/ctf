package com.example.mainproject.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.ScrollView;

import com.example.mainproject.R;
import com.example.mainproject.model.Event;

import java.util.List;

public class EventAdapter extends BaseAdapter {

    private Context context;
    private int layout;
    private List<Event> eventList;

    public EventAdapter(Context context, int layout, List<Event> EventList) {
        this.context = context;
        this.layout = layout;
        this.eventList = EventList;
    }
    @Override
    public int getCount() {
        return eventList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }
    private class ViewHolder{
        TextView txtTitle,txtFormat;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if(convertView==null)
        {
            holder =new ViewHolder();
            LayoutInflater inflater= (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            convertView=inflater.inflate(layout,null);
            holder.txtTitle=(TextView)convertView.findViewById(R.id.textViewTitle);
            holder.txtFormat=(TextView)convertView.findViewById((R.id.textViewFormat));
            convertView.setTag(holder);
        }
        else {
            holder=(ViewHolder) convertView.getTag();
        }
        Event event=eventList.get(position);
        holder.txtTitle.setText("Title: "+event.getTitle());
        holder.txtFormat.setText("Format: "+event.getFormat());
        return convertView;
    }
}
