package com.example.mainproject.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.mainproject.R;

public class ActivitySignup extends AppCompatActivity implements View.OnClickListener {
    private Button btn_linksignin;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        initViews();
        initListeners();
        initObjects();
    }
    public void  initViews(){
        btn_linksignin=(Button)findViewById(R.id.buttonLinkToSignIn2);

    }
    public void initListeners(){
        btn_linksignin.setOnClickListener(this);
    }
    public void initObjects(){

    }

    @Override
    public void onClick(View v) {
        Intent intentSignIn= new Intent(getApplicationContext(),Activity_Signin.class);
        startActivity(intentSignIn);
    }

}
