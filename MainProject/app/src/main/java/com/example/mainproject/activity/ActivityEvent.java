package com.example.mainproject.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.example.mainproject.R;
import com.example.mainproject.adapter.EventAdapter;
import com.example.mainproject.model.Event;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ActivityEvent extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    String url="https://ctftime.org/api/v1/events/";
    ListView listView;
    ArrayList<Event> arrayListEvent;
    EventAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event);
        // get Spinner reference
//        Spinner spinner = (Spinner) findViewById(R.id.spinner);
//        Spinner spinner2 = (Spinner) findViewById(R.id.spinner2);
//        Spinner spinner3 = (Spinner) findViewById(R.id.spinner3);
//        // Spinner click listener
//        spinner.setOnItemSelectedListener(this);
//        spinner2.setOnItemSelectedListener(this);
//        spinner3.setOnItemSelectedListener(this);
//        // Spinner Drop down elements
//        List format = new ArrayList();
//        format.add("Jeopardy");
//        format.add("Attack-Defence");
//        format.add("Hack-quest");
//
//        List location = new ArrayList();
//        location.add("Asia");
//        location.add("Europe");
//        location.add("American");
//        List  year = new ArrayList();
//        year.add("2019");
//        year.add("2018");
//        year.add("2017");
//
//        // Creating array adapter for spinner
//        ArrayAdapter dataAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, format);
//        ArrayAdapter dataAdapter2 = new ArrayAdapter(this, android.R.layout.simple_spinner_item, location);
//        ArrayAdapter dataAdapter3 = new ArrayAdapter(this, android.R.layout.simple_spinner_item, year);
//        // Drop down style will be listview with radio button
//        dataAdapter.setDropDownViewResource(android.R.layout.select_dialog_singlechoice);
//        dataAdapter2.setDropDownViewResource(android.R.layout.select_dialog_singlechoice);
//        dataAdapter3.setDropDownViewResource(android.R.layout.select_dialog_singlechoice);
//
//        // attaching data adapter to spinner
//        spinner.setAdapter(dataAdapter);
//        spinner2.setAdapter(dataAdapter2);
//        spinner3.setAdapter(dataAdapter3);
        listView=(ListView) findViewById(R.id.listviewEvent);
        arrayListEvent=new ArrayList<>();
        adapter=new EventAdapter(this,R.layout.adapter,arrayListEvent);
        listView.setAdapter((adapter));
        getData(url);
    }
    public void getData(String Url)
    {
        final RequestQueue requestQueue= Volley.newRequestQueue(this);
        JsonArrayRequest jsonArrayRequest=new JsonArrayRequest(Request.Method.GET, Url, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        for(int i=0;i<response.length();i++)
                        {
                            try {
                                JSONObject object=response.getJSONObject(i);
                                arrayListEvent.add(new Event(
                                        object.getString("title"),
                                        object.getString("format"),
                                        object.getString("url"),
                                        object.getString("start"),
                                        object.getString("finish")
                                ));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        adapter.notifyDataSetChanged();
                        Toast.makeText(ActivityEvent.this,response.toString(),Toast.LENGTH_LONG).show();

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }

        );
        requestQueue.add(jsonArrayRequest);
    }
    @Override
    public void onItemSelected(AdapterView parent, View view, int position, long id) {
        // getting selected item
        String item = parent.getItemAtPosition(position).toString();

        // Showing selected spinner item in toast
        Toast.makeText(parent.getContext(), "Selected Country: " + item, Toast.LENGTH_LONG).show();

    }

    public void onNothingSelected(AdapterView arg0) {

    }
}
