package com.example.mainproject.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.mainproject.R;

public class Activity_Signin extends AppCompatActivity implements View.OnClickListener {
    private Button btn_link;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity__signin);
        initViews();
        initListeners();
        initObjects();
    }
    public void  initViews(){
        btn_link=(Button)findViewById(R.id.buttonLinkToSignUp);

    }
    public void initListeners(){
        btn_link.setOnClickListener(this);
    }
    public void initObjects(){

    }

    @Override
    public void onClick(View v) {
        Intent intentSignUp= new Intent(getApplicationContext(),ActivitySignup.class);
        startActivity(intentSignUp);
    }
}
