package com.example.ctf.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.ctf.R;
//import com.example.ctf.database.DatabaseHelper;
//import com.example.ctf.model.User;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {
    private final AppCompatActivity activity = RegisterActivity.this;    private EditText e_name ;
    private EditText e_username;
    private EditText e_email;
    private EditText e_password;
    private EditText e_confirmpassword;
    private Button btn_register;
    private TextView t_linkLogin;
   // private User user;
  //  private DatabaseHelper databaseHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        initViews();
        initListeners();
        //initObjects();
    }
    private void initViews(){
        e_name=(EditText)findViewById(R.id.textInputEditTextName);
        e_username=(EditText)findViewById(R.id.textInputEditTextUsername);
        e_email=(EditText)findViewById(R.id.textInputEditTextEmail);
        e_password=(EditText)findViewById(R.id.textInputEditTextPassword);
        e_confirmpassword=(EditText)findViewById(R.id.textInputEditTextConfirmPassword);
        t_linkLogin=(TextView) findViewById(R.id.textViewLinkLogin);
        btn_register=(Button)findViewById(R.id.buttonRegister);

    }
    private void initListeners(){
        btn_register.setOnClickListener(this);
        t_linkLogin.setOnClickListener(this);
    }
//    private void initObjects() {
//        databaseHelper = new DatabaseHelper(activity);
//        user = new User();
//    }
    @Override
    public void onClick(View v) {
//        switch (v.getId()) {
//
//            case R.id.buttonRegister:
//                postDataToSQLite();
//                break;
//
//            case R.id.textViewLinkLogin:
//                finish();
//                break;
//        }
    }
    public String md5(String plaintext) throws NoSuchAlgorithmException {
        String generatedPassword = null;
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(plaintext.getBytes());
            byte[] bytes = md.digest();
            //This bytes[] has bytes in decimal format;
            //Convert it to hexadecimal format
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < bytes.length; i++) {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            //Get complete hashed password in hex format
            generatedPassword = sb.toString();
        }
        catch (NoSuchAlgorithmException e)
        {
            e.printStackTrace();
        }
        return  generatedPassword;

    }
//    private void postDataToSQLite()
////    {
////        String name=e_name.getText().toString().trim();
////        String username=e_username.getText().toString().trim();
////        String email=e_email.getText().toString().trim();
////        String password=e_password.getText().toString().trim();
////        String confirm=e_confirmpassword.getText().toString().trim();
////        if (name.isEmpty()||username.isEmpty()||email.isEmpty()||password.isEmpty()||confirm.isEmpty()){
////            Toast.makeText(this,"Please enter full fields",Toast.LENGTH_SHORT).show();
////            return;
////        }
////        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()){
////            Toast.makeText(this,"Email invalid",Toast.LENGTH_SHORT).show();
////            return;
////        }
////        if(!password.equals(confirm)){
////            Toast.makeText(this,"Password does not match",Toast.LENGTH_SHORT).show();
////            return;
////        }
////        if(!databaseHelper.checkUser(username))
////        {
////            user.setName(name);
////            user.setUsername(username);
////            user.setEmail(email);
////            try {
////                user.setPassword(md5(password));
////            } catch (NoSuchAlgorithmException e) {
////                e.printStackTrace();
////            }
////            databaseHelper.addUser(user);
////            Toast.makeText(this,"Register Successfully",Toast.LENGTH_SHORT).show();
////            emptyInputEditText();
////
////        }
////
////    }
    private void emptyInputEditText() {
        e_name.setText(null);
        e_username.setText(null);
        e_email.setText(null);
        e_password.setText(null);
        e_confirmpassword.setText(null);
    }

}
