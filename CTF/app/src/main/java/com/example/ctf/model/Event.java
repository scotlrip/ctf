package com.example.ctf.model;

import java.util.Date;

public class Event {
    private int id;
    private String format;
    private String url;
    private String name;
    private Date start;
    private Date end;

    public Event(int id, String format, String url, String name, Date start, Date end) {
        this.id = id;
        this.format = format;
        this.url = url;
        this.name = name;
        this.start = start;
        this.end = end;
    }

    public int getId() {
        return id;
    }

    public String getFormat() {
        return format;
    }

    public String getUrl() {
        return url;
    }

    public String getName() {
        return name;
    }

    public Date getStart() {
        return start;
    }

    public Date getEnd() {
        return end;
    }
}
