package com.example.ctf.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.ctf.R;
import com.example.ctf.activity.RegisterActivity;
//import com.example.ctf.database.DatabaseHelper;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    private final AppCompatActivity activity = LoginActivity.this;
    private EditText e_username;
    private  EditText e_password;
    private Button btn_login;
    private TextView t_linkRegister;
    //private DatabaseHelper databaseHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initViews();
        initListeners();
        initObjects();
    }
    public void  initViews(){
        e_username=(EditText)findViewById(R.id.textInputEditTextUsername);
        e_password=(EditText)findViewById(R.id.textInputEditTextPassword);
        t_linkRegister=(TextView)findViewById(R.id.textViewLinkRegister) ;
        btn_login=(Button)findViewById(R.id.buttonLogin);
    }
    private void initListeners() {
        t_linkRegister.setOnClickListener(this);
        btn_login.setOnClickListener(this);
    }
    private void initObjects(){
       // databaseHelper = new DatabaseHelper(activity);

    }

    @Override

    public void onClick(View v) {
        switch (v.getId()) {
        case R.id.buttonLogin:
           // verifyFromSQLite();
            break;
        case R.id.textViewLinkRegister:
            // Navigate to RegisterActivity
            Intent intentRegister = new Intent(getApplicationContext(), RegisterActivity.class);
            startActivity(intentRegister);
            break;
    }
}


//    private void verifyFromSQLite()
//    {
//        String user=e_username.getText().toString().trim();
//        String pass=e_password.getText().toString().trim();
//        if (user.isEmpty()|| pass.isEmpty())
//        {
//          Toast.makeText(this,"Please enter username or password",Toast.LENGTH_SHORT).show();
//        }
//        else {
//            if(databaseHelper.checkUser(user,md5(pass))){
//                Intent accountsIntent = new Intent(activity, IndexActivity.class);
//                accountsIntent.putExtra("USERNAME", user);
//                Toast.makeText(this,"Successfully",Toast.LENGTH_SHORT).show();
//                startActivity(accountsIntent);
//            }
//            else {
//                Toast.makeText(this,"Username or password incorrect",Toast.LENGTH_SHORT).show();
//            }
//        }
//
//    }

    private String md5(String plaintext) {
        String generatedPassword = null;
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(plaintext.getBytes());
            byte[] bytes = md.digest();
            //This bytes[] has bytes in decimal format;
            //Convert it to hexadecimal format
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < bytes.length; i++) {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            //Get complete hashed password in hex format
            generatedPassword = sb.toString();
        }
        catch (NoSuchAlgorithmException e)
        {
            e.printStackTrace();
        }
        return  generatedPassword;
    }

    private void emptyInputEditText() {
        e_username.setText(null);
        e_password.setText(null);
    }
}
